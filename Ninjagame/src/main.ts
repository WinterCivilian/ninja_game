import * as THREE from 'three';
import { GLTFLoader} from 'three/addons/loaders/GLTFLoader.js';
import Hammer from 'hammerjs';
import { Application, Assets } from 'pixi.js';
import { addBackground } from './addBackground';

// Create a PixiJS application.
const app = new Application();
async function setup()
{
    // Intialize the application.
    await app.init({ background: '#1099bb', resizeTo: window });

    // Then adding the application's canvas to the DOM body.
    document.body.appendChild(app.canvas);
}
// Getting the background
async function preload()
{
    // Create an array of asset data to load.
    const assets = [
        { alias: 'background', src: 'https://pixijs.com/assets/tutorials/fish-pond/pond_background.jpg' }
    ];

    // Load the assets defined above.
    await Assets.load(assets);
}

(async () =>
  {
      await setup();
      await preload();
  
      addBackground(app);
  })();

// Set the scene and camera
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

//const renderer = new THREE.WebGLRenderer();
const renderer = new THREE.WebGLRenderer( { alpha: true } );
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// Set the limits of the screen
renderer.domElement.style.position = 'absolute';
renderer.domElement.style.top = '0';
renderer.domElement.style.left = '0';
renderer.domElement.style.width = '100%';
renderer.domElement.style.height = '100%';


// Animation mixer
let mixer: THREE.AnimationMixer;
let model: THREE.Object3D;  // Declare the model variable
let action: THREE.AnimationAction; 

// Get the ninja model and texture
const loader = new GLTFLoader();
const textureLoader = new THREE.TextureLoader();
const ninjaTexture = textureLoader.load('./ninja.png');

// Load the character model
loader.load("./cibus_ninja.glb", (gltf) => {
  model = gltf.scene;
  mixer = new THREE.AnimationMixer(model);
  scene.add(model);

  // Play the first animation
  action = mixer.clipAction(gltf.animations[0], model);
  action.setLoop(THREE.LoopOnce, 1); // Set animation to play only once

    // Apply the texture to the model's materials
    model.traverse((child) => {
      if (child instanceof THREE.Mesh) {
          if (child.material instanceof THREE.MeshStandardMaterial) {
              child.material.map = ninjaTexture;
              child.material.needsUpdate = true;
          }
      }
  });
});

//lighting
const ambientLight = new THREE.AmbientLight(0x555555);
scene.add(ambientLight);

// add directional light source
const directionalLight = new THREE.DirectionalLight(0xffffff);
directionalLight.position.set(1, 1, 1).normalize();
scene.add(directionalLight);

// Set background color
// This background is intended to be seethrough
renderer.setClearColor( 0x000000, 0 );

// Set camera position
camera.position.z = 7;

// Handle window resizing
window.addEventListener('resize', () => {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
});

function animate() {
  requestAnimationFrame(animate);

  if (mixer) {
    mixer.update(1 / 60); // Update the mixer for the animation
  }

  renderer.render(scene, camera);
}

// Start the animation loop
animate();

// Initialize Hammer.js on the renderer's DOM element
const hammer = new Hammer(renderer.domElement);

hammer.on('pan', (event) => {
  // Move the model based on the pan gesture direction
  if (event.direction === Hammer.DIRECTION_LEFT) {
    model.position.x -= 0.5;
    model.rotation.y = -Math.PI / 2; // Face left
  } else if (event.direction === Hammer.DIRECTION_RIGHT) {
    model.position.x += 0.5;
    model.rotation.y = Math.PI / 2; // Face right
  }  else if (event.direction === Hammer.DIRECTION_UP) {
    model.position.y += 0.5;
    model.rotation.y = Math.PI; // Face the screen
  } else if (event.direction === Hammer.DIRECTION_DOWN) {
    model.position.y -= 0.5;
    model.rotation.y = Math.PI; // Face the screen
  }

    // Play the animation action
    if (action) {
      action.stop(); // Stop any ongoing animation
      action.play(); // Play the animation once
    }
});

document.addEventListener('keydown', (event) => {
  // Move the model and update its rotation
  if (event.key === 'ArrowLeft') {
    model.position.x -= 0.5;
    model.rotation.y = -Math.PI / 2; // Face left
  } else if (event.key === 'ArrowRight') {
    model.position.x += 0.5;
    model.rotation.y = Math.PI / 2; // Face right
  } else if (event.key === 'ArrowUp') {
    model.position.y += 0.5;
    model.rotation.y = Math.PI; // Face the screen
  } else if (event.key === 'ArrowDown') {
    model.position.y -= 0.5;
    model.rotation.y = Math.PI; // Face the screen
  }

  if (action) {
    action.stop(); // Stop any ongoing animation
    action.play(); // Play the animation once
  }

});